//import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.List;
import java.util.concurrent.TimeUnit;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author acerPC
 */
public class Zadatak4 {
    
    @Test
    public void testSimple() throws Exception {
        System.setProperty("webdriver.chrome.driver", "C://webdrivers//chromedriver.exe");
        // Instanciranje WebDriver-a
        WebDriver driver = new ChromeDriver();
        
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        //Navigacija na stranicu
        driver.get("file:///C://Users//acerPC//Desktop//TS_Milos_Petkovic_2018270761/zadatak4.html");
        
        //provera naslova
        // Wait for the page to load, timeout after 2 seconds
        (new WebDriverWait(driver, 2)).until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver d) {
                return d.getTitle().contains("Zadatak 4");
            }
        });
        
        //pronalazenje elemenata
        WebElement ime = driver.findElements(By.xpath("//input")).get(0);
        WebElement prezime = driver.findElements(By.xpath("//input")).get(1);
        WebElement brojIndeksa = driver.findElements(By.xpath("//input")).get(2);
        WebElement ocena = driver.findElements(By.xpath("//input")).get(3);
        
        
        ime.clear();
        prezime.clear();
        brojIndeksa.clear();
        ocena.clear();
        
        //Popunjavamo nove vrednosti
        ime.sendKeys("Milos");
        prezime.sendKeys("Markovic");
        brojIndeksa.sendKeys("200");
        ocena.sendKeys("6");
        
        //uporedjujemo vrednosti
        assertEquals("Milos", ime.getAttribute("value"));
        assertEquals("Markovic", prezime.getAttribute("value"));
        assertEquals("200", brojIndeksa.getAttribute("value"));
        assertEquals("6", ocena.getAttribute("value"));
        
        WebElement dodaj = driver.findElement(By.xpath("//button"));
        
        
        List<WebElement> rows = driver.findElements(By.xpath("//tr"));
        int lenBefore = rows.size();
        dodaj.click();
        rows = driver.findElements(By.xpath("//tr"));
        assertEquals(lenBefore+1, rows.size()); 
        
        
        List<WebElement> values = driver.findElements(By.xpath("//tr[last()]/td"));
        WebElement rowIme = values.get(0);
        WebElement rowPrezime = values.get(1);
        WebElement rowBrojIndeksa = values.get(2);
        WebElement rowOcena = values.get(3);
        
        //Uporedjivanje dodate nove vrednosti
        assertEquals(rowIme.getText(), ime.getAttribute("value"));
        assertEquals(rowPrezime.getText(), prezime.getAttribute("value"));
        assertEquals(rowBrojIndeksa.getText(), brojIndeksa.getAttribute("value"));
        assertEquals(rowOcena.getText(), ocena.getAttribute("value"));
        
        
        lenBefore = rows.size();
        
        List<WebElement> deletedElement = driver.findElements(By.xpath("//tr[2]/td"));
        WebElement deletedID = deletedElement.get(0);
        WebElement deletedButton = driver.findElement(By.xpath("//tr[2]/td/a"));
        
        
        deletedButton.click();
        
        rows = driver.findElements(By.xpath("//tr"));
        assertEquals(lenBefore-1, rows.size()); 
        
        
        List<WebElement> secondElement = driver.findElements(By.xpath("//tr[2]/td"));
        WebElement secondElementId = secondElement.get(1);
        
        
        
        assertNotEquals(secondElementId.getText(), deletedID.getText());

        //Close the browser
       //driver.quit();
    }
    
}