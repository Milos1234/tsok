/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z1;


public class University {
    public int grade(int firstTest, int secondTest, int finalExam){
        int grade = 0;
        int combined = firstTest + secondTest + finalExam;
        if((firstTest > 0 && firstTest < 15) || (secondTest >0 && secondTest < 15) || (combined < 51)){
            grade = 0;
        }
        else if(combined < 71){
            grade = 1;
        }
        else{
            grade = 2;
        }
        
        
        return grade;
    }
}
