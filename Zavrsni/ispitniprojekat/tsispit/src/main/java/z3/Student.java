/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z3;


public class Student {
    private String fname;
    private String lname;
    private String ID;

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public Student() {
    }

    public Student(String fname, String lname, String ID) {
        this.fname = fname;
        this.lname = lname;
        this.ID = ID;
    }
    
    
}
