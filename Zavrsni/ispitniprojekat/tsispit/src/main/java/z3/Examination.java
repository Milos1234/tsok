/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z3;


public class Examination {
    private Examiner assistant;
    private Examiner professor;
    private Student student;

    public Examiner getAssistant() {
        return assistant;
    }

    public void setAssistant(Examiner assistant) {
        this.assistant = assistant;
    }

    public Examiner getProfessor() {
        return professor;
    }

    public void setProfessor(Examiner professor) {
        this.professor = professor;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
    
    public void finalExam(){
        assistant.practicalTest(student);
        professor.oralTest(student);
        student = new Student("Marko", "Markovic", "1");
        professor.evaluate(student);
        assistant.evaluate(student);
    }
}
