/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z3;


public interface Examiner {
    public void practicalTest(Student s);
    public void oralTest(Student s);
    public void evaluate(Student s);
}
