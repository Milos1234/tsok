/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z2;


public class GradingService {
    public int gradePaper(int correct, int incorrect, boolean active){
        int score = 0;
        int grade = 0;
        
        if(correct < incorrect){
            score = 0;
        }
        else if(correct == incorrect){
            score = 50;
        }
        
        if(active){
            score = correct * 5;
            score -= incorrect * 2;
        }else{
            score = correct * 4;
            score -= incorrect * 3;
        }
        
        if(score <= 50){
            grade = 0;
        }
        else if(score <=60){
            grade = 1;
        }
        else if(score <=70){
            grade = 2;
        }
        else if(score <=80){
            grade = 3;
        }
        else{
            grade = 4;
        }
        
        return grade;
    }
}
