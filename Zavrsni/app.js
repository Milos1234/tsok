var app = angular.module('myApp', []);
app.controller('MyCtrl', function ($scope) {
    var vm = this;
    vm.obj = {
        ime: '',
        prezime: "",
        brindeks: "",
        ocena: 0
    }

    vm.add = function () {
        var el = {
            ime: vm.obj.ime,
            prezime: "Markovic",
            brindeks: vm.obj.brindeks,
            ocena: vm.obj.ocena
        }
        vm.kolekcija.push(el);
        }

    vm.brisi = function (el) {
        var lista = [];
        for (let i = 0; i < vm.kolekcija.length; i++) {
            //if (vm.vina[i] != el) {
            if(i != vm.kolekcija.length-1){
                lista.push(vm.kolekcija[i]);
            }
        }
        vm.kolekcija = lista;
    }

    vm.kolekcija = [
        { ime: 'Petar', prezime: "Obradovic", brindeks: "ABC1123", ocena:7},
        { ime: 'Marko', prezime: "Pejic", brindeks: "BB32C1", ocena:9},
        { ime: 'Stevan', prezime: "Pavlovic", brindeks: "277334", ocena:6},
    ]

    
});
