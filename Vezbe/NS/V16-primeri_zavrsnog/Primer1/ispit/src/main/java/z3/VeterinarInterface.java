/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z3;


public interface VeterinarInterface {
    public void izvrsiPregled(Krava k);
    public void posaljiPodatke(Krava k);
    public void pregledRezultataAnalize(Krava k);
    public void zapisiDijagnozu(Krava k);
}
