/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z2;


public class BovineTransport {
    
    
    public int calculateCost(int numberOfCows, int distance, boolean express, boolean abroad){
        int price = 500;
        if (abroad){
            price += 200;
            if(distance > 500 && distance < 1000){
                price *=1.5;
            }else if(distance >=1000){
                price *=1.7;
            }else{
                price *=1.2;
            }
            if(express){
                price *=2;
            }
        }else{
            if(distance > 700){
                price *=1.2;
            }
            if(distance > 800){
                price *=1.3;
            }
            if(express){
                price *=1.5;
            }
            
        }
        if(price > 3000){
            price *= 0.8;
        }
        price = price * numberOfCows;
        return price;
    }
    
}
