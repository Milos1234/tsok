/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z3;


public class Krava {
    private int ID;
    private int masa;
    private int uzrast;

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getMasa() {
        return masa;
    }

    public void setMasa(int masa) {
        this.masa = masa;
    }

    public int getUzrast() {
        return uzrast;
    }

    public void setUzrast(int uzrast) {
        this.uzrast = uzrast;
    }

    public Krava(int ID, int masa, int uzrast) {
        this.ID = ID;
        this.masa = masa;
        this.uzrast = uzrast;
    }
    
    public Krava(){}
}
