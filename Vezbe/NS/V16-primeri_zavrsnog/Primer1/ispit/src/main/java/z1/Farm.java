/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z1;

public class Farm {
    
    public int bovineEvaluation(int age, int mass, int quality){
        int value = 0;
        if(age < 5 && mass > 200 && quality < 2){
            value = 500;
        }
        else if((age < 5 && mass >200 && quality >=2) || (age >=5 && mass > 200 && quality <2)
                || (age < 5 && mass <=200 && quality <2)){
            value = 300;
        }
        else{
            value = 150;
        }
        
        
        return value;
    }
    
    public int classifyMilk(int percentage){
       int milkClass = 0;
       if(percentage <=15 && percentage >=0){
           milkClass = 0;
       }else if(percentage >=15 && percentage <=30){
           milkClass = 1;
       }
       else if(percentage >30 || percentage <50){
           milkClass = 2;
       }
       else if(percentage >=50 && percentage <=100){
           milkClass = 3;
       }
       else{
           throw new IllegalArgumentException();
       }
       return milkClass;
    }
    
}
