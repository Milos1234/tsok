/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z3;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import static org.mockito.Mockito.*;


public class Pregled {

    private VeterinarInterface glavniVeterinar;
    private VeterinarInterface pomocniVeterinar;

    public VeterinarInterface getGlavniVeterinar() {
        return glavniVeterinar;
    }

    public void setGlavniVeterinar(VeterinarInterface glavniVeterinar) {
        this.glavniVeterinar = glavniVeterinar;
    }

    public VeterinarInterface getPomocniVeterinar() {
        return pomocniVeterinar;
    }

    public void setPomocniVeterinar(VeterinarInterface pomocniVeterinar) {
        this.pomocniVeterinar = pomocniVeterinar;
    }
    
    //metoda koju je potrebno testirati
    public void pregled(Krava k){ //pregled
        glavniVeterinar.izvrsiPregled(k);//izvrsava se pregled nad veterinarima
        pomocniVeterinar.izvrsiPregled(k);
        glavniVeterinar.posaljiPodatke(k);
        Krava m = new Krava();
        m.setID(5);
        m.setMasa(50);
        m.setUzrast(2); //pravimo podatke o kravi
        glavniVeterinar.zapisiDijagnozu(m); // i zapisujemo dijagnozu
        glavniVeterinar.pregledRezultataAnalize(k);
        
    }
    
    public static void main(String [] args){
    
        VeterinarInterface veterinar = mock(VeterinarInterface.class);
        VeterinarInterface pomocniVeterinar = mock(VeterinarInterface.class);
        Krava k = new Krava();
        k.setID(1);
        k.setMasa(50);
        k.setUzrast(2);
        
        Pregled pregled = new Pregled();
        pregled.setGlavniVeterinar(veterinar);
        pregled.setPomocniVeterinar(pomocniVeterinar);
        
        pregled.pregled(k);
        
        
        InOrder order = inOrder(veterinar, pomocniVeterinar);
        order.verify(veterinar).izvrsiPregled(k);//Veterinar prvo radi preliminarni pregled krave(izvrsiPregled)
        order.verify(pomocniVeterinar).izvrsiPregled(k);//Zatim pomoccnik veterinara vrsi pregled 
        order.verify(pomocniVeterinar).posaljiPodatke(k);// i salje laboratoriji podatke za analizu(posaljiPodatke).
        order.verify(veterinar).pregledRezultataAnalize(k); //Nakon toga, veterinar pregleda rezultate analize(pregledRezultataAnalize),
        order.verify(veterinar).izvrsiPregled(k); //vrsi jos jednom pregled
        order.verify(veterinar).zapisiDijagnozu(k); //  i zapisuje dijagnozu(zapisiDijagnozu).
        
        //Potrebno je utvrditi sledece stvari:
        //1.Metode se pozivaju odgovarajuci broj puta
        //2.Metode se izvrsavaju navedenim redosledom
        //3.Metode se pozivaju sa istim objektom (preko ID-a krave)
        
        //verifikuje se
        verify(veterinar, times(2)).izvrsiPregled(k);//1.
        verify(pomocniVeterinar, times(1)).izvrsiPregled(k);
        verify(pomocniVeterinar, times(1)).posaljiPodatke(k);
        verify(veterinar, times(1)).pregledRezultataAnalize(k);
        verify(veterinar, times(1)).zapisiDijagnozu(k);
        
        ArgumentCaptor<Krava> captor = ArgumentCaptor.forClass(Krava.class);

       for(Krava x :captor.getAllValues()){   
            if(k.getID() != x.getID()){
                System.out.println("Nije ista krava!");
            }    
       } 
    }
}
