/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Korisnik
 */
import java.util.concurrent.TimeUnit;
import org.junit.Test;
import static org.junit.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.*;  

public class Zadatak4 {
    @Test
    public void testSimple() throws Exception {
        
        System.setProperty("webdriver.chrome.driver", "C://webdrivers//chromedriver.exe");
        // Instanciranje WebDriver-a
        WebDriver driver = new ChromeDriver();
        
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        //Navigacija na stranicu
        driver.get("file:///C://Users//acerPC//Desktop//Milos//Milos456//Treca_godina//Testiranje_Softvera//PrimerZavrsnog//Primer//TS_Milan_Zivlak_2018271196//veb aplikacija/zadatak4.html");
        
        //provera naslova
        // Wait for the page to load, timeout after 2 seconds
        (new WebDriverWait(driver, 2)).until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver d) {
                return d.getTitle().contains("Zadatak 4");
            }
        });
        
        //pronalazenje elemenata
        WebElement id = driver.findElements(By.xpath("//input")).get(0);
        WebElement uzrast = driver.findElements(By.xpath("//input")).get(1);
        WebElement masa = driver.findElements(By.xpath("//input")).get(2);
        WebElement vrednost = driver.findElements(By.xpath("//input")).get(3);
        

        //pobrisemo vrednosti unutar polja, ukoliko postoje
        id.clear();
        uzrast.clear();
        masa.clear();
        vrednost.clear();
        
        //Popunjavamo nove vrednosti
        id.sendKeys("K5");
        uzrast.sendKeys("2");
        masa.sendKeys("200");
        vrednost.sendKeys("1000");
        //uporedjujemo vrednosti
        assertEquals("K5", id.getAttribute("value"));
        assertEquals("2", uzrast.getAttribute("value"));
        assertEquals("200", masa.getAttribute("value"));
        assertEquals("1000", vrednost.getAttribute("value"));
               
                
        //testiranje za dodavanje novog reda
        
        WebElement dodaj = driver.findElement(By.xpath("//button"));
        
        //u toku dodavanja podataka se pojavljuje sledeci red sa podacima
        List<WebElement> rows = driver.findElements(By.xpath("//tr"));
        int lenBefore = rows.size();//trenutni broj redova
        dodaj.click();//na klik se doda novi red
        rows = driver.findElements(By.xpath("//tr"));//dobavljamo redove
        assertEquals(lenBefore+1, rows.size());//uporedjujemo dali je dodat novi red 
        
        //dobavljanje elemenata na novo dodatom redu
        List<WebElement> values = driver.findElements(By.xpath("//tr[last()]/td"));
        WebElement rowId = values.get(0);
        WebElement rowUzrast = values.get(1);
        WebElement rowMasa = values.get(2);
        WebElement rowVrednost = values.get(3);
        
        //Uporedjivanje dodate nove vrednosti
        assertEquals(rowId.getText(), id.getAttribute("value"));
        assertEquals(rowUzrast.getText(), uzrast.getAttribute("value"));
        assertEquals(rowMasa.getText(), masa.getAttribute("value"));
        assertEquals(rowVrednost.getText(), vrednost.getAttribute("value"));
        
        
        lenBefore = rows.size();
        
        List<WebElement> deletedElement = driver.findElements(By.xpath("//tr[2]/td"));//! tr[2] je drugi red gde se elementi nalaze i nepocinje na 0 nego sa 1 
        WebElement deletedID = deletedElement.get(0);//dobavljamo element sa ti ID-em
        WebElement deletedButton = driver.findElement(By.xpath("//tr[2]/td/a"));//dobavljamo dugme 
        //List<WebElement> fullList = driver.findElements(By.xpath("//tr/td"));
        
        deletedButton.click();//brise se red
        
        rows = driver.findElements(By.xpath("//tr"));//dobavljanje redova
        assertEquals(lenBefore-1, rows.size()); //Uporedjivanje da li je izbrisan element i da li toliko ima redova
        
        //dobavljanje elementa na drugom redu
        List<WebElement> secondElement = driver.findElements(By.xpath("//tr[2]/td"));
        WebElement secondElementId = secondElement.get(1);
        
        // Ovdje bi vrijednosti trebale da su razlicite(dobavljam tr[2] i prije i poslije)
        // Dobijem K3 i prije i poslije brisanja, prije brisanja je na 3. a poslije na 2. mjestu.
        assertNotEquals(secondElementId.getText(), deletedID.getText());//da li su vrednosti razlicite
        
        

//        
//        //Close the browser
        //driver.quit();
    }
    
}
