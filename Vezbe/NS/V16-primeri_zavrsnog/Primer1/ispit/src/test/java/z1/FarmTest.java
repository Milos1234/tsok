/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package z1;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Korisnik
 */
public class FarmTest {
    
    public FarmTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of bovineEvaluation method, of class Farm.
     */
    
    @Test
    //vrednost 500,godina < 5,mass > 200,quality < 2
    public void testBovineEvaluation500First() {
        System.out.println("bovineEvaluation");
        int age = 4;
        int mass = 220;
        int quality = 0;
        Farm instance = new Farm();
        int expResult = 500;
        int result = instance.bovineEvaluation(age, mass, quality);
        assertEquals(expResult, result);

    }
    
    @Test
    public void testBovineEvaluation500Second() {
        System.out.println("bovineEvaluation");
        int age = 4;
        int mass = 220;
        int quality = 1;
        Farm instance = new Farm();
        int expResult = 500;
        int result = instance.bovineEvaluation(age, mass, quality);
        assertEquals(expResult, result);

    }
    
    @Test
    public void testBovineEvaluation300First() {
        System.out.println("bovineEvaluation");
        int age = 6;
        int mass = 220;
        int quality = 0;
        Farm instance = new Farm();
        int expResult = 300;
        int result = instance.bovineEvaluation(age, mass, quality);
        assertEquals(expResult, result);

    }
    
    @Test
    public void testBovineEvaluation300Second() {
        System.out.println("bovineEvaluation");
        int age = 6;
        int mass = 220;
        int quality = 1;
        Farm instance = new Farm();
        int expResult = 300;
        int result = instance.bovineEvaluation(age, mass, quality);
        assertEquals(expResult, result);

    }
    
    @Test
    public void testBovineEvaluation300Third() {
        System.out.println("bovineEvaluation");
        int age = 4;
        int mass = 200;
        int quality = 0;
        Farm instance = new Farm();
        int expResult = 300;
        int result = instance.bovineEvaluation(age, mass, quality);
        assertEquals(expResult, result);

    }
    
    @Test
    public void testBovineEvaluation300Forth() {
        System.out.println("bovineEvaluation");
        int age = 4;
        int mass = 200;
        int quality = 1;
        Farm instance = new Farm();
        int expResult = 300;
        int result = instance.bovineEvaluation(age, mass, quality);
        assertEquals(expResult, result);

    }
    
    @Test
    public void testBovineEvaluation150First() {
        System.out.println("bovineEvaluation");
        int age = 7;
        int mass = 220;
        int quality = 2;
        Farm instance = new Farm();
        int expResult = 150;
        int result = instance.bovineEvaluation(age, mass, quality);
        assertEquals(expResult, result);

    }
    
    @Test
    public void testBovineEvaluation150Second() {
        System.out.println("bovineEvaluation");
        int age = 4;
        int mass = 200;
        int quality = 2;
        Farm instance = new Farm();
        int expResult = 150;
        int result = instance.bovineEvaluation(age, mass, quality);
        assertEquals(expResult, result);

    }
    
    @Test
    public void testBovineEvaluation150Third() {
        System.out.println("bovineEvaluation");
        int age = 7;
        int mass = 200;
        int quality = 2;
        Farm instance = new Farm();
        int expResult = 150;
        int result = instance.bovineEvaluation(age, mass, quality);
        assertEquals(expResult, result);

    }
    
    @Test
    public void testBovineEvaluation150Forth() {
        System.out.println("bovineEvaluation");
        int age = 7;
        int mass = 200;
        int quality = 0;
        Farm instance = new Farm();
        int expResult = 150;
        int result = instance.bovineEvaluation(age, mass, quality);
        assertEquals(expResult, result);

    }
    
    @Test
    public void testBovineEvaluation150Fifth() {
        System.out.println("bovineEvaluation");
        int age = 7;
        int mass = 200;
        int quality = 1;
        Farm instance = new Farm();
        int expResult = 150;
        int result = instance.bovineEvaluation(age, mass, quality);
        assertEquals(expResult, result);

    }

    /**
     * Test of classifyMilk method, of class Farm.
     */
    @Test//Milki klasa 0
    public void testClassifyMilkValidZeroClass() {
        System.out.println("classifyMilk");
        int percentage = 2;
        Farm instance = new Farm();
        int expResult = 0;
        int result = instance.classifyMilk(percentage);
        assertEquals(expResult, result);

    }
    
    @Test//Milki klasa 1
    public void testClassifyValidFirstClass() {
        System.out.println("classifyMilk");
        int percentage = 25;
        Farm instance = new Farm();
        int expResult = 1;
        int result = instance.classifyMilk(percentage);
        assertEquals(expResult, result);

    }
    
    @Test//Milki klasa 2
    public void testClassifyValidSecondClass() {
        System.out.println("classifyMilk");
        int percentage = 33;
        Farm instance = new Farm();
        int expResult = 2;
        int result = instance.classifyMilk(percentage);
        assertEquals(expResult, result);

    }
    
    @Test//Milki klasa 3
    public void testClassifyThirdClass() {
        System.out.println("classifyMilk");
        int percentage = 60;
        Farm instance = new Farm();
        int expResult = 3;
        int result = instance.classifyMilk(percentage);
        assertEquals(expResult, result);

    }
    //invalid koji je manji od 0 u farm.java
    @Test(expected = IllegalArgumentException.class)
    public void testClassifyInvalidLessThen0() {
        System.out.println("classifyMilk");
        int percentage = -1;
        Farm instance = new Farm();
        int expResult = 3;
        int result = instance.classifyMilk(percentage);
        //assertEquals(expResult, result);

    }
    //invalid koji je veci od 100 u farm.java
    @Test(expected = IllegalArgumentException.class)
    public void testClassifyInvalidGreaterThen100() {
        System.out.println("classifyMilk");
        int percentage = 110;
        Farm instance = new Farm();
        int expResult = 3;
        int result = instance.classifyMilk(percentage);
        //assertEquals(expResult, result);

    }
    
}
