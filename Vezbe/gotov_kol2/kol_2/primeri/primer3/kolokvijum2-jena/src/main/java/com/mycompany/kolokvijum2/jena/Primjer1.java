/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.kolokvijum2.jena;

import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.SimpleSelector;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

/**
 *
 * @author User
 */
public class Primjer1 {
    
    public static void main(String[] args){
    
     
        Model schema = RDFDataMgr.loadModel("C:\\Users\\User\\Documents\\WBIS\\kolokvijum2-schema.rdf");
        Model data = RDFDataMgr.loadModel("C:\\Users\\User\\Documents\\WBIS\\kolokvijum2-model.rdf");
        
        
        InfModel infModel = ModelFactory.createRDFSModel(schema, data);
        
        Resource v1 = infModel.getResource("http://www.singidunum.ac.rs/voz#v1");
        
        System.out.println("Tipovi za instancu v1 :");
        
        StmtIterator iterator = infModel.listStatements(v1, RDF.type, (RDFNode) null);
        while(iterator.hasNext()){
            Statement s = iterator.nextStatement();
            System.out.println(s.toString());
        }
    
        
        
        System.out.println("");
        
        Resource zaposleniClass = infModel.getResource("http://www.singidunum.ac.rs/voz#zaposleni"); 
        
        
        System.out.println("Selektor za zaposleni : ");
        SimpleSelector zaposleni = new SimpleSelector(null, RDFS.subClassOf, zaposleniClass);
        
        iterator = data.listStatements(zaposleni);
        while(iterator.hasNext()){
            System.out.println(iterator.nextStatement().toString());
        }
        
        
    }
    
}
