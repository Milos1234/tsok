package fabrika;

import java.io.InputStream;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.SimpleSelector;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.riot.RDFDataMgr;

public class fabrika {

	public static void main(String[] args) {
		String pathToRdf = "C:\\Users\\acerPC\\Desktop\\Milos\\Milos456\\Treca_godina\\WBIS\\kol_2\\primeri\\primer2\\fabrika.rdf";
        Model model = ModelFactory.createDefaultModel();
        InputStream in = RDFDataMgr.open(pathToRdf);

        model.read(in, null);
        model.write(System.out);

        Resource serviser = model.getResource("http://www.singidunum.ac.rs/servicer");
        String prefixURI = model.getNsPrefixURI("fct");
        Property serviseri = model.getProperty("http://www.singidunum.ac.rs/serviser#", "serviser");

        SimpleSelector svi = new SimpleSelector(null, null, (RDFNode) null);

        System.out.println("Svi:");
        StmtIterator iterator = model.listStatements(svi);
        while (iterator.hasNext()){
            System.out.println(iterator.nextStatement().toString());
        }

	}

}
