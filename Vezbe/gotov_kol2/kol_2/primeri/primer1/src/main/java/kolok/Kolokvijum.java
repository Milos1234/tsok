/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kolok;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.SimpleSelector;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.riot.RDFDataMgr;

import java.io.InputStream;

public class Kolokvijum {


    public static void main(String[] args) {
        String pathToRdf = "C:\\Users\\acerPC\\Desktop\\Milos\\Milos456\\Treca_godina\\WBIS\\kol_2\\primeri\\primer1\\bolnica.rdf";
        Model model = ModelFactory.createDefaultModel();
        InputStream in = RDFDataMgr.open(pathToRdf);

        model.read(in, null);
        model.write(System.out);

        Resource doktor = model.getResource("http://www.singidunum.ac.rs/hospital/doktor");
        String prefixURI = model.getNsPrefixURI("hsp");
        Property doktori = model.getProperty("http://www.singidunum.ac.rs/hospital#", "doktor");
        
        

        SimpleSelector svi = new SimpleSelector(null, null, (RDFNode) null);

        System.out.println("Svi:");
        StmtIterator iterator = model.listStatements(svi);
        while (iterator.hasNext()){
            System.out.println(iterator.nextStatement().toString());
        }
    }


}
